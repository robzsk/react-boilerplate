import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.js';

import './../style/main.css';

const root = document.getElementById('root');
ReactDOM.render(<App />, root);
